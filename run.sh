#/bin/bash

DSL_DATA_PATH="./dataset/dsl/"
SVG_DATA_PATH="./dataset/svg/"
PNG_DATA_PATH="./dataset/png/"
AUG_PNG_DATA_PATH="./dataset/aug_png/"
MODEL_PATH="./output_model/pix2code.h5"
PREDICT_DSL_PATH="./dataset/predict_dsl/"

python ./model/data_preparation/generate_random_data.py ${DSL_DATA_PATH} 1000

python ./compiler/compiler.py multiple ${DSL_DATA_PATH} ${SVG_DATA_PATH}

python ./model/data_preparation/svg2png.py ${SVG_DATA_PATH} ${PNG_DATA_PATH}

python ./model/data_preparation/imageprocess.py ${PNG_DATA_PATH} ${AUG_PNG_DATA_PATH}

python ./model/train.py ${AUG_PNG_DATA_PATH} ${MODEL_PATH}

python ./model/sample.py ${MODEL_PATH} pix2code ${AUG_PNG_DATA_PATH} ${PREDICT_DSL_PATH}