# Emoji Codifier
The project mainly depends on the idea of [model of #pix2code](https://github.com/tonybeltramelli/pix2code).

## Abstract
Transforming an emoji icon created by a designer into computer code is a typical task conducted by a developer in order to build vectorized and amendable icons.

## Citation

```
@misc{wong2018emojicodifier,
  author = {Pak-Kan Wong, Jinbo Xing, Yuechen Zhang},
  title = {Emoji Codifier},
  year = {2018},
  publisher = {GitLabb},
  journal = {GitLab repository},
  howpublished = {\url{https://gitlab.com/pacowong/emoji-codifier}}
}
```

and

```
@article{beltramelli2017pix2code,
  title={pix2code: Generating Code from a Graphical User Interface Screenshot},
  author={Beltramelli, Tony},
  journal={arXiv preprint arXiv:1705.07962},
  year={2017}
}
```

## Disclaimer

The following software is shared for educational purposes only. 
The author and its affiliated institution are not responsible in any manner whatsoever for any damages, including any direct, indirect, special, incidental, or consequential damages of any character arising as a result of the use or inability to use this software.

The research project demonstrates an application of deep neural networks to generate code from visual inputs.
This project is experimental and shared for educational purposes only.
Both the source code and the datasets are provided to foster future research in machine intelligence and are not designed for end users.

## Setup
### Prerequisites

- Python 3
- pip

### Install dependencies

```sh
pip install -r  requirements.txt
```

## Emoji dataset
The emoji dataset can be obtained from [Emojipedia](https://emojipedia.org/)

## Usage
Follow the instruction in [#pix2code](https://github.com/tonybeltramelli/pix2code)

To run the program
```sh
bash run.sh
```

