#!/usr/bin/env python
#! encoding:UTF-8
import cairosvg
import sys
import os

def exportsvg(fromDir, targetDir, exportType):

    num = 0
    for a,f,c in os.walk(fromDir):
        for fileName in c:
            path = os.path.join(a,fileName)
            if os.path.isfile(path) and fileName[-3:] == "svg":
                num += 1
                fileHandle = open(path)
                svg = fileHandle.read()
                fileHandle.close()
                exportPath = os.path.join(targetDir, fileName[:-3] + exportType)
                exportFileHandle = open(exportPath,'w')

                if exportType == "png":
                    try:
                        cairosvg.svg2png(bytestring=svg, write_to=exportPath)
                    except:
                        continue

                exportFileHandle.close()

if __name__ == '__main__':
    argv = sys.argv[1:]
    if len(argv) == 2:
        svgDir = sys.argv[1]
        exportDir = sys.argv[2]
        exportFormat = 'png'
        if not os.path.exists(exportDir):
            os.mkdir(exportDir)
        exportsvg(svgDir, exportDir, exportFormat)
    else:
        print("Usage: python svg2png.py svg_input_dir png_output_dir")
        exit(-1)